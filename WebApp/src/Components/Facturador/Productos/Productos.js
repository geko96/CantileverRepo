import { useContext, useState, useEffect } from "react";
import { Button, Table, Navbar, Container, Nav, Modal, Form } from "react-bootstrap";
import { contexto } from "../../Context/Context";
import './Productos.css';
import Swal from 'sweetalert2';



export default function Productos() {
    const {loged, user, token, ApiURL} = useContext(contexto);
    const [show, setShow] = useState(false);
    const [productos, setProductos] = useState([])
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    useEffect(() => {
        fetch(`${ApiURL}/products/list`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                "owner": user.cuit,
                "token": token
            })
        }).then(res => res.json())
        .then(data => {
            console.log(data)
            setProductos(data)
        })
    }, [])

    ///update product list every 5 seconds


    

    function CheckProducts () {
      fetch(`${ApiURL}/products/list`, {
          method: 'POST',
          headers: {
              'Content-Type': 'application/json'
          },
          body: JSON.stringify({
              "owner": user.cuit,
              "token": token
          })
      }).then(res => res.json())
      .then(data => {
          console.log(data)
          setProductos(data)
      })
    }

    
  

    


    function saveProduc () {
      let name = document.getElementById('txtNombre').value
      let price = document.getElementById('txtPrice').value
      let description = document.getElementById('txtDescription').value
      let category = document.getElementById('txtCategoria').value
      let stock = document.getElementById('txtStock').value
      let owner = user.cuit
      let provider = document.getElementById('txtProvider').value
      let code = document.getElementById('txtCode').value
      let iva = document.getElementById('txtIVA').value

      ///chech if the values are not empty
      if (name === '' || price === '' ||  code === '') {
        Swal.fire({
          icon: 'error',
          title: 'Oops...',
          text: 'Todos los campos son obligatorios!',
        })
      } else {
        ///save new product
        const product = {
          name,
          price,
          description,
          category,
          stock,
          owner,
          provider,
          token,
          code,
          iva
        }

        fetch(ApiURL+'/products', {
          method: 'POST',
          body: JSON.stringify(product),
          headers: {
            'Content-Type': 'application/json'
          }
        })
          .then(res => res.json())
          .then(data => {
            if (data) {
              Swal.fire(

                'Producto guardado!',
                'El producto se guardó correctamente.',
                'success'
              )
            }
          }).catch(err => {
            console.log(err)
          })

          
      }
      handleClose()

    }

    function deleteProduct (code) {
      fetch(ApiURL+'/products', {
        method: 'DELETE',
        body: JSON.stringify({
          "owner": user.cuit,
          "code": code,
          "token": token
        }),
        headers: {
          'Content-Type': 'application/json'
        }
      })
        .then(res => res.json())
        .then(data => {
          if (data) {
            Swal.fire(

              'Producto eliminado!',
              'El producto se eliminó correctamente.',
              'success'
            )
          }
          CheckProducts()
        }).catch(err => {
          console.log(err)
        })
    }

    
    
    if(productos.length > 0){
      return (
        <div className="productos">
            <Navbar bg="dark" variant="dark">
              <Container>
              <Navbar.Brand>Productos</Navbar.Brand>
              <Nav className="justify-content-end">
                <Nav.Link><Button variant="secondary" className="margin10" onClick={
                  () => {
                    setShow(true)
                  }
                }><img src='/circulo-plus.png' className="plusIcon" alt="New"/></Button></Nav.Link>
                <Form className="d-flex">
                  <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                  />
                  
                </Form>

                
              </Nav>
              </Container>
            </Navbar>
            <p></p>

            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Nuevo Producto</Modal.Title>
              </Modal.Header>
              <Modal.Body>
              <div class="inputGroup">
                <div class="groupText">
                    <label for="txtNombre">Nombre</label>
                    <input type="text" id="txtNombre" name="txtNombre" placeholder="Nombre"/>
                </div>
                <div class="groupText">
                    <label for="txtCode">Codigo</label>
                    <input type="text" id="txtCode" name="txtCode" placeholder="Barcode"/>
                </div>
                <div class="groupText">
                    <label for="txtIVA">IVA</label>
                    <input type="number" id="txtIVA" name="txtIVA" placeholder="IVA"/>
                </div>
                <div class="groupText">
                    <label for="txtPrice">Precio</label>
                    <input type="number" id="txtPrice" name="txtPrice" placeholder="Precio"/>
                </div>
                <div class="groupText">
                    <label for="txtCategoria">Categoria</label>
                    <input type="text" id="txtCategoria" name="txtCategoria" placeholder="Categoria"/>
                </div>
                <div class="groupText">
                    <label for="txtStock">Stock</label>
                    <input type="number" id="txtStock" name="txtStock" placeholder="Stock"/>
                </div>
                <div class="groupText">
                    <label for="txtProvider">Proveedor</label>
                    <input type="text" id="txtProvider" name="txtProvider" placeholder="Proveedor"/>
                </div>
                <div class="groupText">
                  <label for="txtDescription">Descripcion</label>
                  <textarea type="text" id="txtDescription" name="txtDescription" placeholder="Descripcion"/>
              </div>
            </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                  Close
                </Button>
                <Button variant="primary" onClick={() => {
                  saveProduc()
                  handleClose()
                  CheckProducts()
                }}>
                  Save Changes
                </Button>
              </Modal.Footer>
            </Modal>

            <Table striped bordered hover variant="dark">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Precio Principal</th>
                  <th>Otros Precios</th>
                  <th>Stock</th>
                  <th>IVA</th>
                  <th>Proveedores</th>
                  <th><i class="bi bi-pencil-square"></i></th>
                </tr>
              </thead>
              <tbody>

              {
                productos.map((producto, index) => {
                  return (
                    <tr>
                      <td>{index+1}</td>
                      <td>{producto.name}</td>
                      <td>${producto.price}</td>
                      <td><Button>Otros Precios</Button></td>
                      <td>{producto.stock}</td>
                      <td>{producto.iva}%</td>
                      <td><Button>Proveedores</Button></td>
                      <td><Button variant="secondary" onClick={()=>{
                        deleteProduct(producto.code)
                        CheckProducts()
                      }}><i class="bi bi-pencil-square"></i></Button></td>
                    </tr>
                  )
                })
              }              

              </tbody>
            </Table>
        </div>
        
    )
    }else{
      return (
        <div className="productos">
            <Navbar bg="dark" variant="dark">
              <Container>
              <Navbar.Brand>Productos</Navbar.Brand>
              <Nav className="justify-content-end">
                <Nav.Link><Button variant="dark" onClick={
                  () => {
                    setShow(true)
                  }
                }>Nuevo</Button></Nav.Link>
                <Nav.Link><Button variant="dark">Editar</Button></Nav.Link>
                
              </Nav>
              </Container>
            </Navbar>
            <p></p>

            <Modal show={show} onHide={handleClose}>
              <Modal.Header closeButton>
                <Modal.Title>Nuevo Producto</Modal.Title>
              </Modal.Header>
              <Modal.Body>
              <div class="inputGroup">
                <div class="groupText">
                    <label for="txtNombre">Nombre</label>
                    <input type="text" id="txtNombre" name="txtNombre" placeholder="Nombre"/>
                </div>
                <div class="groupText">
                    <label for="txtCode">Codigo</label>
                    <input type="text" id="txtCode" name="txtCode" placeholder="Barcode"/>
                </div>
                <div class="groupText">
                    <label for="txtIVA">IVA</label>
                    <input type="number" id="txtIVA" name="txtIVA" placeholder="IVA"/>
                </div>
                <div class="groupText">
                    <label for="txtPrice">Precio</label>
                    <input type="number" id="txtPrice" name="txtPrice" placeholder="Precio"/>
                </div>
                <div class="groupText">
                    <label for="txtCategoria">Categoria</label>
                    <input type="text" id="txtCategoria" name="txtCategoria" placeholder="Categoria"/>
                </div>
                <div class="groupText">
                    <label for="txtStock">Stock</label>
                    <input type="number" id="txtStock" name="txtStock" placeholder="Stock"/>
                </div>
                <div class="groupText">
                    <label for="txtProvider">Proveedor</label>
                    <input type="text" id="txtProvider" name="txtProvider" placeholder="Proveedor"/>
                </div>
                <div class="groupText">
                  <label for="txtDescription">Descripcion</label>
                  <textarea type="text" id="txtDescription" name="txtDescription" placeholder="Descripcion"/>
              </div>
            </div>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleClose}>
                  Close
                </Button>
                <Button variant="primary" onClick={() => {
                  saveProduc()
                  handleClose()
                  CheckProducts()

                }}>
                  Save Changes
                </Button>
              </Modal.Footer>
            </Modal>

            <div>No hay Productos</div>
        </div>
        
    )
    }
    
}