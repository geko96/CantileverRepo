import { useContext, useState, useEffect } from "react";
import { contexto } from "../Context/Context";
import './Landing.css';
import { Navbar, NavDropdown, Nav, Container, Button, Table, Form, Modal } from 'react-bootstrap';
import thisImage from './circulo-plus.png';


export default function Welcome() {
    const {loged, setLoged, user, setUser, ApiURL, token} = useContext(contexto);
    console.log(user);

    const [cajas, setCajas] = useState([]);
    const [loading, setLoading] = useState(true);
    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    function pullCajas() {
        fetch(`${ApiURL}/fcpoints?owner=${user.cuit}&token=${token}`, {
            method: 'GET',
            headers: {
                'Content-Type': 'application/json',

            },
            params: {
                owner: user.cuit,
                token: token
            },
            query: {
                owner: user.cuit,
                token: token
            }

        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
            setCajas(data);
            setLoading(false);
        })
    }

    
    useEffect(() => {
      setCajas(pullCajas());
    }, [])

    function saveFCPoint (e){
      const name = document.getElementById('name').value;
      const owner = user.cuit;
      const location = document.getElementById('location').value;
      const email = document.getElementById('email').value;
      const cajas = document.getElementById('cajas').value;
      const status = document.getElementById('status').value;

      const data = {
        name,
        owner,
        location,
        email,
        cajas,
        status,
        token

      };

      ///check if values are empty
      if(!name || !owner || !location || !email || !cajas || !status) {
        return alert('empty fields');
        e.preventDefault();
      };

      fetch(`${ApiURL}/fcpoints`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      }).then(()=>{
        pullCajas();
      })
    }

    function deleteFCPoint (id) {
      fetch(`${ApiURL}/fcpoints`, {
        method: 'DELETE',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({id, token}),
      }).then(()=>{
        pullCajas();
      })
    }
    
    if(!loading) {
    return (
        <>
            <h1 className="welcomeBanner">Bienvenido {user.name}</h1>
            <div className="productos">
            <Navbar bg="dark" variant="dark">
              <Container>
              <Navbar.Brand>Puntos de facturación</Navbar.Brand>
              <Nav className="justify-content-end">
                <Nav.Link className="margin10"><Button variant="dark" onClick={()=>{
                  handleShow();
                }}><img src={thisImage} className="plusIcon" alt="New"/></Button></Nav.Link>
                <Form className="d-flex">
                  <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                  />
                  
                </Form>
                
              </Nav>
              </Container>
            </Navbar>
            <p></p>

            <Table striped bordered hover variant="dark">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nombre</th>
                  <th>Direccion</th>
                  <th>Cajas</th>
                  <th>Estatus</th>
                  <th><i class="bi bi-pencil-square"></i></th>
                </tr>
              </thead>
              <tbody>
              {
                cajas.map((caja, index) => {
                  return (
                    <tr>
                      <td>{index+1}</td>
                      <td>{caja.name}</td>
                      <td>{caja.location}</td>
                      <td>{caja.cajas}</td>
                      <td>{caja.status}</td>
                      
                      <td><Button variant="secondary" onClick={()=>{
                        deleteFCPoint(caja._id)
                      }}><i class="bi bi-pencil-square"></i></Button></td>
                    </tr>
                  )
                })
              }   

              </tbody>
            </Table>
        </div>
        <Modal show={show} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Agregar Punto de facturacion</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div className="form-group">
              <label for="name">Nombre</label>
              <input type="text" className="form-control" id="name" placeholder="Nombre del punto de facturacion"/>
              <label for="location">Direccion</label>
              <input type="text" className="form-control" id="location" placeholder="Direccion del punto de facturacion"/>
              <label for="email">Correo</label>
              <input type="email" className="form-control" id="email" placeholder="Correo del punto de facturacion"/>
              <label for="cajas">Cajas</label>
              <input type="number" className="form-control" id="cajas" placeholder="Cantidad de cajas"/>
              <label for="status">Estatus</label>
              <select className="form-control" id="status">
                <option>Activo</option>
                <option>Inactivo</option>
              </select>
              
            </div>
            
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Close
            </Button>
            <Button variant="primary" onClick={() => {
              saveFCPoint();
              handleClose();
            }}>
              Save Changes
            </Button>
          </Modal.Footer>
        </Modal>
        </>
    )
    }
}