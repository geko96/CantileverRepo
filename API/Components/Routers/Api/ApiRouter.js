

var express = require('express');
var ApiRouter = express.Router();

const Login = require('../../Layers/Login');
const Register = require('../../Layers/Register');
const ListProducts = require('../../Layers/Products/List');
const saveFCPoint = require('../../Layers/FCPoints/SaveFCPoints');
const ListFCPoints = require('../../Layers/FCPoints/ListFCPoints');
const DeleteFCpoint = require('../../Layers/FCPoints/DeleteFCpoint');

const Save = require('../../Layers/Products/Save');
const FindOne = require('../../Layers/Products/FindOne');
const deleteOne = require('../../Layers/Products/DeleteOne');

ApiRouter.post('/register', Register);
ApiRouter.post('/login', Login);

ApiRouter.route('/products')
.post(Save)
.get(FindOne)
.delete(deleteOne)


ApiRouter.route('/products/list')
.post(ListProducts)

ApiRouter.route('/fcpoints')
.post(saveFCPoint)
.get(ListFCPoints)
.delete(DeleteFCpoint)





module.exports = ApiRouter