require('dotenv').config();

const mongoose = require('mongoose');

const URL = process.env.MONGODB_URI;

const connection = mongoose.connect(URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
})

module.exports = connection;