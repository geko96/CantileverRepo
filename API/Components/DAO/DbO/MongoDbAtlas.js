const db = require('../../Database/MongoDbAtlas.js');
const FacturationPointModel = require('../../Models/FacturationPoint');
require('dotenv').config();

class MongoDbAtlas {
    constructor() {
        if(DbHost === 'mongodb') {
            this.db = db;
            this.FacturationPointModel = FacturationPointModel;
        }
        
        
    }

    

    async save(FCPoint) {
        //// check if the FCPoint already exists with name and owner
        const FCPointExists = await this.FacturationPointModel.findOne({name: FCPoint.name, owner: FCPoint.owner});
        if(FCPointExists) return false;
        const newFCPoint = await this.FacturationPointModel.create(FCPoint);
        return newFCPoint;
    }

    ///view all FCPoints
    List(data) {
        const FCPoin = this.FacturationPointModel.find({owner: data});
        
        return FCPoin;

    }

    async Delete(id) {
        const product = await this.FacturationPointModel.findByIdAndDelete(id);
        console.log(id);
        return product;
    }
        
    

    
}

module.exports = MongoDbAtlas;