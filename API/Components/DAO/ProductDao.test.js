const ProductDao = require('./ProductDao');
const assert = require('assert').strict;


const dao = new ProductDao();

const product = {
    name: "ters",
    code: "123" ,
    stock: 1,
    price: "1",
    description: "1", 
    iva: 1,
    category: 1, 
    image: 1,
    owner: 20396121442,
    provider: 1, 
};

let code = "123";

describe('Test sobre ProductDao', () => {
    describe('.List(Owner)', () => {
        it('Retorna los productos del propietario 20396121442', async () => {
            const products = await dao.List(product.owner);
            assert.ok(products.length > 0);
        });
    });
    describe('.save(product)', () => {
        it('Guarda un producto', async () => {
            
            const savedProduct = await dao.save(product);
            assert.ok(savedProduct);
        });
    });
    describe('.findOne(owner, code)', () => {
        it('Busca un producto', async () => {
            const foundProduct = await dao.findOne(product.owner, code);
            assert.ok(foundProduct);
        });
    });
    describe('.deleteOne(owner, code)', () => {
        it('Elimina un producto', async () => {
            const deletedProduct = await dao.deleteOne(product.owner, code);
            assert.ok(deletedProduct);
        });
    });
    
});
