const db = require('../Database/MongoDbAtlas.js');
const productModel = require('../Models/Product');


class ProductDao {
    constructor() {
        this.db = db;
        this.productModel = productModel;
    }

    async save(product) {
        const newProduct = await this.productModel.create(product);
        return newProduct;
    }

    async findOne(owner, code) {
        const product = await this.productModel.findOne({owner, code});
        return product;
    }

    async List(owner) {
        const products = await this.productModel.find({owner});
        return products;

    }

    ///delete by code
    async deleteOne(owner, code) {
        const product = await this.productModel.findOneAndDelete({owner, code});
        return product;
    }
}

module.exports = ProductDao;