
require('dotenv').config();
const MongoDbAtlas = require('./DbO/MongoDbAtlas');

const DbHost = process.env.DB_HOST || 'mongodb';
console.log("Database Host: " +DbHost);

function DbFactory() {
    if(DbHost === 'mongodb') {
        return new MongoDbAtlas();
    }

    return new MongoDbAtlas();
}


class FacturationPointDao extends DbFactory {
    constructor() {
        super();
    }
}


module.exports = FacturationPointDao;