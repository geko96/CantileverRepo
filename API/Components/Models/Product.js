
const { Schema , model } = require('mongoose');


const ProductSchema = new Schema({
    name: {type: String, required: true},
    code: {type: String, required: false},
    stock: {type: Number, required: false},
    price: {type: String, required: false},
    description: {type: String, required: false},
    iva: {type: Number, required: false},
    category: {type: String, required: false},
    image: {type: String, required: false},
    owner: {type: String, required: true},
    provider: {type: String, required: false},
});


module.exports = model('Product', ProductSchema);