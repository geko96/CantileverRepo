
const { Schema , model } = require('mongoose');


const FacturationPointSchema = new Schema({
    name: {type: String, required: true},
    owner: {type: Number, required: false},
    location: {type: String, required: false},
    email: {type: String, required: true},
    cajas: {type: Number, required: false},
    status: {type: String, required: false},
    key: {type: String, required: false},
    cert: {type: String, required: false},

});


module.exports = model('FacturationPoint', FacturationPointSchema);