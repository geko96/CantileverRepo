

function Login (req, res) {
    const db = require('../Database/db');
    const userModel = require('../Models/User');

    const jwt = require('jsonwebtoken');

    const SECRET = 'secret';

    const { mail, password } = req.body
    db.then(db => {
        userModel.findOne({mail: mail, password: password}, (err,user) => {
            if(err) return res.send(err)
            if(!user) return res.send('user not found')

            const token = jwt.sign({user}, SECRET, { expiresIn: '365d' })

            

            let session = {
                id: user._id,
                name: user.name,
                mail: user.mail,
                cuit: user.cuit,
                role: user.role,
                token: token

            }

            
            
            return res.send(session)
        })
    }).catch(err => console.log(err))
}


module.exports = Login