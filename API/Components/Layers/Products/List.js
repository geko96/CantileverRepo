

async function ListProducts( req, res ) {
    const ProductDao = require('../../DAO/ProductDao');
    const container = new ProductDao();
    

    const {owner, token} = req.body;
    let products = await container.List(owner);

    const secret = process.env.SECRET;
    const jwt = require('jsonwebtoken');
    ////verify token
    jwt.verify(token, secret, (err, decoded) => {
        if(err) return res.status(401).send('unauthorized')
        if(decoded) {
            //list products
            
            return res.send(products)
            
        }
    });
    
    
}

module.exports = ListProducts;