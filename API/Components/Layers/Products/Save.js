const ProductDao =require('../../DAO/ProductDao');

function Save( req, res ) {
    const container = new ProductDao();

    const secret = process.env.SECRET;
    const jwt = require('jsonwebtoken');


    const { token, name, stock, price, description, category, image, owner, provider, code, iva } = req.body;
    //check if any field is empty
    if(!name || !stock || !price || !description || !code) return res.status(400).send('empty fields');
    //save new product
    const product = {
        name,
        price,
        description,
        category,
        stock,
        image,
        iva,
        owner,
        provider,
        code,
    }

    ///check if token is valid
    jwt.verify(token, secret, (err, decoded) => {
        if(err) return res.status(401).send('unauthorized')
        if(decoded) {
            container.save(product, (err, product) => {
                if(err) return res.status(400).send(err)
                return res.send(product)
            })
            res.send(product)
        }
    });

    

}


module.exports = Save;