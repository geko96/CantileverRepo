const ProductDao = require('../../DAO/ProductDao');
const container = new ProductDao();

function FindOne( req, res ) {
    const {owner, code} = req.body;
    
    return res.send(container.findOne(owner, code));
}

module.exports = FindOne;