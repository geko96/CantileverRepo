const ProductDao = require('../../DAO/ProductDao');
const container = new ProductDao();

function DeleteOne( req, res ) {
    ///check if token is valid
    const { token, owner, code } = req.body;
    const secret = process.env.SECRET;
    const jwt = require('jsonwebtoken');

    jwt.verify(token, secret, (err, decoded) => {
        if(err) return res.status(401).send('unauthorized')
        if(decoded) {
            container.deleteOne(owner, code, (err, product) => {
                if(err) return res.status(400).send(err)
                return res.send(product)
            })
        }
    });
}


module.exports = DeleteOne;