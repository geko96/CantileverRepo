

function info (req,res) {
    require('dotenv').config();
    const parseArgs = require('minimist');

    const args = parseArgs(process.argv.slice(2));
    let data = {
        "name": "Api Cantilever",
        "Sistema operativo": process.platform,
        "Total de nucleos": require('os').cpus().length,
        "Versión de Node": process.version,
        "Memoria total reservada": process.memoryUsage().heapTotal,
        "Path de ejecucion": process.execPath,
        "Process id": process.pid,
        "Carpeda del proyecto": __dirname,
    }
    res.json(data)
}

module.exports = info