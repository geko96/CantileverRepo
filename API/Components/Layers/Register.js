function Register (req, res) {
    const db = require('../Database/db');
    const userModel = require('../../Components/Models/User');

    const jwt = require('jsonwebtoken');

    const SECRET = 'secret';
    const { name, password, mail, cuit, } = req.body
    
    const user = {
        name,
        password,
        mail,
        cuit,
        role: 'AdminUser'
    }

    

    db.then(db => {
        userModel.create(user, (err,user) => {
            if(err) return res.status(400).send(err)
            return res.send(user)
        })
    }).catch(err => console.log(err))
}


module.exports = Register