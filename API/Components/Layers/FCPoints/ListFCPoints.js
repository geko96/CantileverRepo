const FacturationPointDao = require('../../DAO/FacturationPointDAO');

function ListFCPoints (req, res) {
    const container = new FacturationPointDao();

    const secret = process.env.SECRET;
    const jwt = require('jsonwebtoken');

    const { token, owner } = req.query;

    //check if any field is empty
    if(!owner) return res.status(400).send('empty fields');
    //save new FCPoint
    

    ///check if token is valid
    jwt.verify(token, secret, (err, decoded) => {
        if(err) return res.status(401).send('unauthorized')
        if(decoded) {
            let data = container.List(owner);
            data.then((FCPoints) => {
                res.status(200).send(FCPoints);
            })
        }
    });

    

}


module.exports = ListFCPoints;