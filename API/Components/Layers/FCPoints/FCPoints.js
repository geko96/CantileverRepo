const FacturationPointDao = require('../../DAO/FacturationPointDAO');

function saveFCPoint (req, res) {
    const container = new FacturationPointDao();

    const secret = process.env.SECRET;
    const jwt = require('jsonwebtoken');

    const { token, name, owner, credentials, location, status, email } = req.body;
    //check if any field is empty
    if(!name || !owner) return res.status(400).send('empty fields');
    //save new FCPoint
    const FCPoint = {
        name,
        owner,
    }

    ///check if token is valid
    jwt.verify(token, secret, (err, decoded) => {
        if(err) return res.status(401).send('unauthorized')
        if(decoded) {
            container.save(FCPoint, (err, FCPoint) => {
                if(err) return res.status(400).send(err)
                return res.send(FCPoint)
            })
            res.send(FCPoint)
        }
    });

    

}