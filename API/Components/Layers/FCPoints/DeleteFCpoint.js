const FacturationPointDao = require('../../DAO/FacturationPointDAO');


function DeleteFCpoint (req, res) {
    const {id, token} = req.body;
    const container = new FacturationPointDao();



    ////check if token is valid
    const secret = process.env.SECRET;
    const jwt = require('jsonwebtoken');
    jwt.verify(token, secret, (err, decoded) => {
        if(err) return res.status(401).send('unauthorized')
        if(decoded) {
            container.Delete(id)
            res.status(200).send('deleted');
        }
    });


}

module.exports = DeleteFCpoint;