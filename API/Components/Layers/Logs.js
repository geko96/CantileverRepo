const log4js = require('log4js');

log4js.configure({
    appenders: {
        consola: { type: 'console'},
        warning: { type: 'file', filename: '../../logs/warn.log'},
        error: { type: 'file', filename: '../../logs/error.log'},
        info: { type: 'file', filename: '../../logs/info.log'},
    },
    categories: {
        default: { appenders: ['consola', 'warning', 'error'], level: 'debug' },
        info: { appenders: ['info','consola'], level: 'info' },
        error: { appenders: ['error'], level: 'error' },
        warning: { appenders: ['warning'], level: 'warn' },
    }
})

const warn = log4js.getLogger('warning');
const error = log4js.getLogger('error');
const info = log4js.getLogger('info');


function LogMiddleware(req, res, next) {
    console.log(
        JSON.stringify({
            "Origin IP": req.ip,
            "url": req.url,
            "method": req.method,
            })
        )
    
    next();
}   

module.exports = {
    warn,
    error,
    info,
    LogMiddleware
}