require('dotenv').config();
const https = require('https');
const fs = require('fs');

const express = require('express');
const app = express();
const {LogMiddleware} = require('./Components/Layers/Logs');
const ApiRouter = require('./Components/Routers/Api/ApiRouter');
const AdminRouter = require('./Components/Routers/Admin/Admin')
/// import port from minimist



const cors = require('cors');

const PORT = process.env.PORT || 443;

app.use(cors({
    origin: '*'
}));

//use public folder for static files with ___dirname
app.use(express.static(__dirname + '/public'));


app.use(express.json())
app.use(express.urlencoded({ extended:true }))


app.use(LogMiddleware);
   
app.use('/api', ApiRouter)
app.use('/admin', AdminRouter)



app.get('/not-found', (req,res) => {
    warn.warn('url Not found')
    res.status(404).send('Not found')

})



const server = https.createServer({
    key: fs.readFileSync(`/etc/letsencrypt/live/cantilever.com.ar-0001/privkey.pem`, 'utf8'),
    cert: fs.readFileSync(`/etc/letsencrypt/live/cantilever.com.ar-0001/fullchain.pem`, 'utf8')
  }, app)
  
  
server.listen(443, () => {
    console.log(`Server is listening on port ${PORT}`);
}).on('error', err => {
    console.log('Error en e servidor:', err)
})

