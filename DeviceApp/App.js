import React from 'react';
import { Provider } from 'react-redux';
import EntryPoint from './Components/EntryPoint/EntryPoint';

import store from "./Components/Store/Index";

export const DEV_MODE = false;


export default function App() {


  
return (
  <Provider store={store}>
    <EntryPoint />
  </Provider>
)
  
  
}


