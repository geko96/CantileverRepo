import { configureStore } from "@reduxjs/toolkit";
import UserSlice from "./Slice/User.slice";
import ProductSlice from "./Slice/Product.slice";


const store = configureStore({
    reducer: {
        User: UserSlice,
        Product: ProductSlice,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware({
        serializableCheck: false,
    }),
});


export default store;