import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    List: [],
};

const ProductSlice = createSlice({
    name: "Product",
    initialState,
    reducers: {
        setProducts: (state, action) => {{
            let actualList = state.List;
            actualList.push(action.payload);
            state.List = actualList;
        }},

    },
});


export const { setProducts } = ProductSlice.actions;


export default ProductSlice.reducer;