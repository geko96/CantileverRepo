import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    userID: null,
    userName: null,
    token: null,
    isAuth: false,
    FullUser: null,
};

const UserSlice = createSlice({
    name: "User",
    initialState,
    reducers: {
        setUserID: (state, action) => {{
            state.userID = action.payload;
        }},
        setUserName: (state, action) => {{
            state.userName = action.payload;
        }},
        setToken: (state, action) => {{
            state.token = action.payload;
        }},
        setIsAuth: (state, action) => {{
            state.isAuth = action.payload;
        }},
        setFullUser: (state, action) => {{
            state.FullUser = action.payload;
        }}
    },
});

export const { setUserID, setUserName, setToken, setIsAuth, setFullUser } = UserSlice.actions;


export default UserSlice.reducer;