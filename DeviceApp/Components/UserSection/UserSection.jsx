import {View, Text, StyleSheet, StatusBar, TouchableOpacity} from "react-native";
import { useSelector, useDispatch } from "react-redux";
import { setUserID, setUserName, setToken, setIsAuth } from "../Store/Slice/User.slice";

export default function UserSection () {

    const user = useSelector(state => state.User);
    console.log(user);


    const dispatch = useDispatch();

    return (
        <View style={styles.container}>
            <View style={styles.userSection}>
                <Text style={styles.Title}>{user.userName}</Text>
                <View style={styles.UserSection}>
                    <Text style={styles.UserSectionText}>ID: {user.userID}</Text>
                    <Text style={styles.UserSectionText}>CUIT: {user.FullUser.cuit}</Text>
                </View>
                <View style={styles.PlanSection}>
                    <Text style={styles.Plan}>Plan Contratado:</Text>
                    <Text style={styles.PlanCenter}>Comercio 1</Text>
                    <Text style={styles.Plan}>Servicios incluidos:</Text>
                    <Text style={styles.PlanCenter2}>* Facturacion electronica</Text>
                    <Text style={styles.PlanCenter2}>* Stock</Text>
                    <Text style={styles.PlanCenter2}>* Ventas</Text>
                    <Text style={styles.PlanCenter2}>* Compras</Text>
                    <Text style={styles.PlanCenter2}>* Clientes</Text>
                    <Text style={styles.PlanCenter2}>* Proveedores</Text>
                    <Text style={styles.PlanCenter2}>* Reportes</Text>
                    <Text style={styles.PlanCenter2}>* Caja</Text>
                </View>
            </View>
            
            
            <TouchableOpacity style={styles.LogOut} onPress={() => {
                dispatch(setIsAuth(false));
                dispatch(setToken(""));
                dispatch(setUserName(""));
                dispatch(setUserID(""));
                console.log('Logged out');
            }}>
                <Text style={styles.subTitle}>Salir</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "column",
        flexWrap: "nowrap",
        paddingTop: StatusBar.currentHeight,
        backgroundColor: '#FFFCC9',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    Title: {
        fontSize: 30,
        fontFamily: 'PoppinsBold',
        textAlign: 'center',
        width: '100%',
        marginTop: 10,
        color: '#43464B',

    },
    subTitle: {
        fontSize: 20,
        fontFamily: 'PoppinsMedium',
        textAlign: 'center',
        width: '100%',
    },
    LogOut: {
        backgroundColor: '#BFC8D6',
        width: '100%',
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
        marginBottom: 10,

    },
    UserSection: {
        flexDirection: "column",
        width: '100%',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        textAlign: 'left',
        height: 100,
        marginTop: 10,
    },
    UserSectionText: {
        fontSize: 20,
        fontFamily: 'PoppinsMedium',
        textAlign: 'left',
        paddingLeft: 10,
        marginTop: 10,
        width: '100%',
        backgroundColor: '#B2BEB5',
        paddingVertical: 10,
        color: '#3B3D35',

    },
    PlanSection: {
        flexDirection: "column",
        width: '90%',
        justifyContent: 'flex-start',
        alignItems: 'flex-start',
        textAlign: 'left',
        backgroundColor: '#B2BEB5',
        marginTop: 40,
        padding: 10,


    },
    Plan: {
        fontSize: 20,
        fontFamily: 'PoppinsMedium',
        textAlign: 'left',

        paddingLeft: 25,
        marginTop: 10,
        width: '100%',
        paddingVertical: 10,
        color: '#3B3D35',

    },
    PlanCenter: {
        fontSize: 35,
        fontFamily: 'PoppinsBold',
        textAlign: 'center',
        width: '80%',
        marginHorizontal: '10%',
        borderRadius: 10,
        color: '#BFC8D6',
        backgroundColor: '#05C3DD',

    },
    PlanCenter2: {
        fontSize: 15,
        fontFamily: 'PoppinsBold',
        textAlign: 'center',
        width: '80%',
        marginHorizontal: '10%',

    },
    userSection: {
        width: '100%',
        height: 'auto',
        alignItems: 'center',
        justifyContent: 'center',


    },

    
});