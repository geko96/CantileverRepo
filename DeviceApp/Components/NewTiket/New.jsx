import {View, Text, Image, StyleSheet, Button, Platform, Dimensions, ScrollView, FlatList} from 'react-native';
import { useState } from 'react';
import React from 'react';
import Scan from './Scanner';
import Item from './Items';
import ItemGroup from './ItemGroup';





export default function New() {
    const [ItemArray, setItemArray] = useState([]);
    
    
    return (
        <View style={styles.container}>
            <View style={styles.TopGroup}>
                <Text style={styles.amountText}>$ 0.00</Text>
                <Scan />       
            </View>

            <ScrollView style={styles.ScrollView}>
                <ItemGroup />
            </ScrollView>
        </View>
        
      );
    
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 8,
        backgroundColor: "aliceblue",
        },

    ScrollView: {
        backgroundColor: "lightblue",
        flexDirection: 'column',
        width: '100%',
        height: '100%',
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
        color: '#008080',
        marginBottom: 20,
        width: '100%',
    }, 
    amountText: {
        
        fontSize: 40,
        color: '#008080',
        padding: 10,
        width: '70%',
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 15,
        textAlign: 'center',
        textAlignVertical: 'center',
        height: 100,

    },
    TopGroup: {
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        width: '90%',
        marginLeft: '5%',
        marginVertical: 10,
        },
});