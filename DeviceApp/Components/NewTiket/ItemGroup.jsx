import {View, Text, StyleSheet, Button, FlatList} from 'react-native';
import { useState, useEffect } from 'react';
import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import Item from './Items';
import { setProducts } from '../Store/Slice/Product.slice';



export default function ItemGroup() {
    
    const dispatch = useDispatch();
    let products = useSelector(state => state.Product.List);
   

    // Function delete by timestamp
    function deleteItem(timeStamp) {
        let newList = products.filter(item => item.timeStamp !== timeStamp);
        dispatch(setProducts(newList));
        
    }

    


    if (products  <= 0) {
        
        return(
            <View style={styles.container}>
                <Text style={styles.title}>No Items</Text>
            </View>

        )
    }else{
        console.log(products)
        return(
            <View style={styles.container}>
                <FlatList
                inverted
                data={products}
                renderItem={Item}
                keyExtractor={item => JSON.stringify(item.timeStamp)}
                style={styles.list}
            />
            </View>
        )
        
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    title: {
        fontSize: 30,
        fontWeight: 'bold',
    },
    list: {
        width: '90%',
        alignSelf: 'center',
        marginVertical: 10,
    },

});