import {View, Text, StyleSheet, Button} from 'react-native';
import { useState, useEffect } from 'react';
import React from 'react';
import { AntDesign } from '@expo/vector-icons'; 


export default function Item({item}) {

    return (
        <View style={styles.container}>
            <View style={styles.title}>
                <Text style={styles.titleText}>Name: {item.name}</Text>
                <View style={styles.iconRight}>
                    <AntDesign name="delete" size={24} color="black" />
                </View>
            </View>
            <View style={styles.title}>
                <Text style={styles.titleText}>Price: {item.price}</Text>
                <View style={styles.iconRight}>
                    <AntDesign name="shoppingcart" size={24} color="black" />
                    <Text style={styles.text}>{item.quantity}</Text>
                </View>
            </View>
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        borderWidth: 1,
        borderColor: 'green',
        borderRadius: 15,
        fontWeight: 'bold',
        padding: 10,
        marginVertical: 10,
    },
    title: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        fontSize: 20,
        color: '#008080',
        width: '100%',
        marginTop: 10,
    },
    titleText: {
        fontSize: 20,
        color: '#008080',
        width: '50%',
        textAlign: 'left',

    },
    titleTextRight: {
        fontSize: 20,
        color: '#008080',
        textAlign: 'right',
    },
    iconRight: {
        flexDirection: 'row',
        alignContent: 'flex-end',
        justifyContent: 'flex-end',
        textAlignVertical: 'center',

    },
    text: {
        fontSize: 20,
        
    },
});