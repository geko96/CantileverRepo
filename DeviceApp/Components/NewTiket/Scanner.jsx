
import { Camera, CameraType } from 'expo-camera';
import {View, Text, Image, StyleSheet, Button, Platform, Dimensions, TouchableOpacity, Modal} from 'react-native';
import { useState, useEffect } from 'react';
import React from 'react';
import { setProducts } from '../Store/Slice/Product.slice';
import { useDispatch, useSelector } from 'react-redux';


export default function Scan() {

    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);
    const [modalVisible, setModalVisible] = useState(false);

    const [type, setType] = useState(CameraType.back);
    const [permission, requestPermission] = Camera.useCameraPermissions();


    function toggleCameraType() {
        setType((current) => (
          current === CameraType.back ? CameraType.front : CameraType.back
        ));
    }
    
    



    const dispatch = useDispatch();



    const handleBarCodeScanned = ({ type, data }) => {
        setScanned(true);
        //dispatch(setProducts(data))
        saveCode({"id":data,"timeStamp":Date.now(),"name":"algo","price":1.99,"quantity":1})
        
    };

    

    function saveCode(code) {
        dispatch(setProducts(code))
    }


    function AutoScan() {
        setTimeout(() => {
        
            setScanned(false);
        }, 1000);
    }

    

    return(
        <View style={styles.container}>
            
            <Camera style={styles.camera} 
                type={type}
                flashMode={Camera.Constants.FlashMode.torch}
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}>

                    <View style={styles.buttonContainer}>
                    
                        <TouchableOpacity onPress={ toggleCameraType} style={styles.button}>
                            <Text style={styles.text}> Flip </Text>
                        </TouchableOpacity>
                       
                    </View>
            </Camera>
            
                
            {scanned && AutoScan()}

            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        marginTop: 8,
    },
    absoluteFillObject: {
        ...StyleSheet.absoluteFillObject,
        width: '100%',
        height: '100%',

    }, 
    camera: {
        flex: 1,
        width: '100%',
        height: '100%',
        margin: 8,  

    },
    button: {
        height: '100%',
        width: '100%',

    },
    text: {
        fontSize: 50,
        color: 'transparent',
        height: '100%',
        width: '100%',
        textAlign: 'center',
        textAlignVertical: 'center',

    },

});